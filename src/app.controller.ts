import type { Response } from 'express';
import { join } from 'path';
import { createReadStream } from 'fs';
import { Controller, Get, Res, StreamableFile } from '@nestjs/common';

@Controller()
export class AppController {
  @Get('/file')
  async downloadFile(@Res({ passthrough: true }) res: Response) {
    try {
      const filename = 'test.csv';

      const readStream = createReadStream(join(process.cwd(), filename));

      res.set({
        'Content-Type': 'text/csv',
        'Content-Disposition': 'attachment; filename="' + filename + '"',
      });

      // res.send(readStream)

      return new StreamableFile(readStream);

    } catch (error) {
      throw new Error(error)
    }
  }
}
